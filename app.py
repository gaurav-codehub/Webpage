from flask import Flask, render_template, request
from datetime import datetime
app = Flask(__name__)


def start():
    head = "Artificial Intellegence. Lets begin!"
    year = datetime.now().year
    year = year == 2020
    name1 = ["CAR", "PEN", "CHAIR", "KITE"]
    return render_template("start1.html", headline= head, yyear= year, name1=name1)

@app.route('/')
def home():
    return render_template("home.html") 
    
@app.route('/next',methods= ["GET", "POST"])
def next():
    if request.method == "GET":
        return "Please submit the report!"
    else:   
        name = request.form.get("name")
        return render_template("next_page.html", name = name) 

@app.route('/next1')
def next1():
    return render_template("next_page.html") 