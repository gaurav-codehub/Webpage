# Doesn't work
from flask_sqlalchemy import SQLAlchemy
import os
db = SQLAlchemy()

class Flight(db.Model):
    __tablename__ = "flights"
    id = db.Column(db.Integer, primary_key = True)
    origin = db.Column(db.String, nullable = False)
    destination = db.Column(db.String, nullable = False)
    duration = db.Column(db.Integer, nullable = False)

class Passanger(db.Model):
    __tablename__ = "passanger"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False)
    flight_id = db.Column(db.Integer, db.ForeignKey("flight.id"), nullable = False)
    